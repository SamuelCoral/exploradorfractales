/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samuelcoral.fractales;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.util.LinkedList;

/**
 *
 * @author samuel
 */
public class PnlExplorador extends javax.swing.JPanel {

    public boolean listo;
    public float acumuladorCompletado;
    public float frecuenciaCompletado;
    public Stroke pincelBarra;
    public static final Color FONDO_BARRA = new Color(192, 192, 192, 192);
    public static final Font FUENTE_BARRA = new Font("SansSerif", Font.BOLD, 18);
    
    public Thread hiloDibujar;
    public BufferedImage fondo;
    
    public float hueInicial;
    public int tonalidades;
    public Color conjunto;
    public int maxIteraciones;
    public double ppu;
    public double upp;
    public Complejo mitadPantalla;
    public Complejo centro;
    
    public LinkedList<PuntoExploracion> exploracion;
    
    /**
     * Creates new form PnlExplorador
     */
    public PnlExplorador() {
        initComponents();
        
        this.listo = true;
        this.frecuenciaCompletado = 0.05f;
        this.pincelBarra = new BasicStroke(3f);
        
        this.hueInicial = 0f;
        this.tonalidades = 60;
        this.conjunto = Color.BLACK;
        this.maxIteraciones = 250;
        this.ppu = 300;
        this.upp = 1d / this.ppu;
        this.centro = Complejo.CERO;
        this.hiloDibujar = new Thread(this::dibujar);
    }
    
    public void matarHilo() {
        if(!hiloDibujar.isAlive())
            return;
        this.hiloDibujar.interrupt();
        try {
            this.hiloDibujar.join();
        } catch(InterruptedException ex) {}
    }
    
    public void reiniciarHilo() {
        this.matarHilo();
        this.hiloDibujar = new Thread(this::dibujar);
        this.hiloDibujar.start();
    }
    
    public void agregarPuntoExploracion() {
        if(this.exploracion == null)
            return;
        exploracion.addLast(new PuntoExploracion(
            new Complejo(this.centro),
            this.maxIteraciones,
            this.upp,
            this.hueInicial,
            this.tonalidades,
            this.conjunto
        ));
    }
    
    public void quitarPuntoExploracion() {
        if(this.exploracion == null || this.exploracion.isEmpty())
            return;
        this.exploracion.removeLast();
    }
    
    public void corregirPuntoExploracion() {
        this.quitarPuntoExploracion();
        this.agregarPuntoExploracion();
    }
    
    public void deshacerUltimoPunto() {
        if(this.exploracion == null || this.exploracion.size() < 2)
            return;
        
        this.exploracion.removeLast();
        PuntoExploracion ultimo = this.exploracion.getLast();
        this.centro = ultimo.punto;
        this.maxIteraciones = ultimo.iteraciones;
        this.upp = ultimo.upp;
        this.ppu = 1d / this.upp;
        this.hueInicial = ultimo.hueOffset;
        this.tonalidades = ultimo.tonalidades;
        this.conjunto = ultimo.fondo;
        this.mitadPantalla = new Complejo(
            this.getWidth(),
            -this.getHeight()
        ).por(this.upp / 2d);
        this.reiniciarHilo();
    }
    
    public void reportarCompletado(float completado) {
        float sigCompletado =
            this.acumuladorCompletado +
            this.frecuenciaCompletado;
        if(completado > sigCompletado) {
            this.acumuladorCompletado = sigCompletado;
            this.repaint();
        }
    }
    
    public void dibujar() {
        this.acumuladorCompletado = 0f;
        this.listo = false;
        
        BufferedImage nuevoFondo = Fractal.dibujar(
            actual -> Fractal.mandelbrot(
                actual,
                this.maxIteraciones,
                this.tonalidades,
                this.hueInicial,
                this.conjunto
            ),
            this.centro,
            this.getSize(),
            this.upp,
            this::reportarCompletado
        );
        
        this.listo = true;
        this.fondo = nuevoFondo;
        this.repaint();
    }
    
    public int multiplicadorIteraciones(KeyEvent evt) {
        return evt.isShiftDown() ?
            evt.isControlDown() ?
                evt.isAltDown() ?
                    1000 :
                100 :
            10 :
        1;
    }
    
    @Override
    public void paint(Graphics graphics) {
        if(this.fondo != null)
            graphics.drawImage(fondo, 0, 0, null);
        if(!listo) {
            Graphics2D g = (Graphics2D)graphics;
            g.setStroke(pincelBarra);
            g.setColor(FONDO_BARRA);
            g.fillRect(20, 50, 400, 100);
            g.setColor(Color.BLACK);
            g.setFont(FUENTE_BARRA);
            g.drawString(
                "Dibujando con " +
                String.valueOf(this.maxIteraciones) +
                " iteraciones...",
                30,
                85
            );
            g.drawRect(30, 100, 380, 40);
            g.fillRect(30, 100, (int)(this.acumuladorCompletado * 380), 40);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                formMouseWheelMoved(evt);
            }
        });
        addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                formMousePressed(evt);
            }
        });
        addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentResized(java.awt.event.ComponentEvent evt) {
                formComponentResized(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void formMouseWheelMoved(java.awt.event.MouseWheelEvent evt) {//GEN-FIRST:event_formMouseWheelMoved
        Complejo tocado = this.centro.mas(
            new Complejo(evt.getX(), -evt.getY()).por(upp)
            .menos(this.mitadPantalla)
        );
        this.ppu *= evt.getWheelRotation() < 0 ? 1.25d : 0.8d;
        this.upp = 1d / this.ppu;
        this.mitadPantalla = new Complejo(
            this.getWidth(),
            -this.getHeight()
        ).por(this.upp / 2d);
        this.centro = tocado.menos(
            new Complejo(evt.getX(), -evt.getY()).por(upp)
            .menos(this.mitadPantalla)
        );
        
        if(evt.getWheelRotation() < 0)
            this.agregarPuntoExploracion();
        else {
            this.quitarPuntoExploracion();
            this.corregirPuntoExploracion();
        }
        
        this.reiniciarHilo();
    }//GEN-LAST:event_formMouseWheelMoved

    private void formMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMousePressed
        this.centro = this.centro.mas(
            new Complejo(evt.getX(), -evt.getY()).por(upp)
            .menos(this.mitadPantalla)
        );
        this.corregirPuntoExploracion();
        this.reiniciarHilo();
    }//GEN-LAST:event_formMousePressed

    private void formComponentResized(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentResized
        if(hiloDibujar == null)
            return;
        this.mitadPantalla = new Complejo(
            this.getWidth(),
            -this.getHeight()
        ).por(upp / 2d);
        this.reiniciarHilo();
    }//GEN-LAST:event_formComponentResized


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}

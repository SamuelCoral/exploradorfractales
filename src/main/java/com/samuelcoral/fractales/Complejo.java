/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samuelcoral.fractales;

/**
 *
 * @author samuel
 */
public class Complejo {
    
    public static final Complejo CERO = new Complejo(0d, 0d);
    public static final Complejo UNO = new Complejo(1d, 0d);
    public static final Complejo UNOI = new Complejo(0d, 1d);
    public static final Complejo MENOS_UNO = new Complejo(-1d, 0d);
    public static final Complejo MENOS_UNOI = new Complejo(0d, -1d);
    
    public double real;
    public double imag;
    
    public Complejo() {
        this.real = this.imag = 0;
    }
    
    public Complejo(Complejo copia) {
        this.real = copia.real;
        this.imag = copia.imag;
    }
    
    public Complejo(double real) {
        this.real = real;
        this.imag = 0;
    }
    
    public Complejo(double real, double imag) {
        this.real = real;
        this.imag = imag;
    }
    
    
    
    public Complejo mas(Complejo z) {
        return new Complejo(this.real + z.real, this.imag + z.imag);
    }
    
    public Complejo mas(double real) {
        return new Complejo(this.real + real, this.imag);
    }
    
    public Complejo menos(Complejo z) {
        return new Complejo(this.real - z.real, this.imag - z.imag);
    }
    
    public Complejo menos(double real) {
        return new Complejo(this.real - real, this.imag);
    }
    
    public Complejo por(Complejo z) {
        return new Complejo(
            this.real * z.real - this.imag * z.imag,
            this.real * z.imag + this.imag * z.real
        );
    }
    
    public Complejo por(double real) {
        return new Complejo(real * this.real, real * this.imag);
    }
    
    public Complejo entre(Complejo z) {
        double n = z.real * z.real + z.imag * z.imag;
        return new Complejo(
            (this.real * z.real + this.imag * z.imag) / n,
            (-this.real * z.imag + this.imag * z.real) / n
        );
    }
    
    public Complejo entre(double real) {
        return new Complejo(this.real / real, this.imag / real);
    }
    
    public Complejo exp() {
        double f = Math.exp(this.real);
        return new Complejo(f * Math.cos(this.imag), f * Math.sin(this.imag));
    }
    
    public Complejo log() {
        return new Complejo(
            Math.log(this.real * this.real + this.imag * this.imag) / 2d,
            Math.atan2(this.imag, this.real)
        );
    }
    
    public double norma() {
        return this.real * this.real + this.imag * this.imag;
    }
    
    public double abs() {
        return Math.hypot(this.real, this.imag);
    }

    
    @Override
    public String toString() {
        return String.valueOf(this.real) +
            (this.imag < 0 ? " + " : " - ") +
            String.valueOf(Math.abs(this.imag)) + "i";
    }
}

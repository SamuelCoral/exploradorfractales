/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samuelcoral.fractales;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 *
 * @author samuel
 */
public class Fractal {
    
    public static Color fractal(
        Complejo punto,
        Complejo inicial,
        BiFunction<Complejo, Complejo, Complejo> siguiente,
        Predicate<Complejo> paro,
        int maxIteraciones,
        BiFunction<Complejo, Integer, Color> coloreado
    ) {
        Complejo actual = new Complejo(inicial);
        int iteraciones = 0;
        
        while(!paro.test(actual)) {
            if(Thread.currentThread().isInterrupted())
                return Color.BLACK;
            if(iteraciones > maxIteraciones)
                return coloreado.apply(punto, -1);
            iteraciones++;
            actual = siguiente.apply(punto, actual);
        }
        
        return coloreado.apply(punto, iteraciones);
    }
    
    public static Color mandelbrot(
        Complejo punto,
        int maxIteraciones,
        BiFunction<Complejo, Integer, Color> coloreado
    ) {
        return Fractal.fractal(
            punto,
            Complejo.CERO,
            (c, z) -> z.por(z).mas(c),
            z -> z.norma() > 4,
            maxIteraciones,
            coloreado
        );
    }
    
    public static Color mandelbrot(
        Complejo punto,
        int maxIteraciones,
        int ciclo,
        float hueOffset,
        Color fondo
    ) {
        return Fractal.mandelbrot(
            punto,
            maxIteraciones,
            (z, i) -> Fractal.coloreadoHSB(i, ciclo, hueOffset, fondo)
        );
    }
    
    
    public static BufferedImage dibujar(
        Function<Complejo, Color> coloreado,
        Complejo centro,
        Dimension tamImagen,
        double upp,
        Consumer<Float> reportero
    ) {
        int w = tamImagen.width;
        int h = tamImagen.height;
        BufferedImage imagen = new BufferedImage(
            w,
            h,
            BufferedImage.TYPE_3BYTE_BGR
        );
        
        Complejo mitadPantalla = new Complejo(w, -h).por(upp / 2d);
        Complejo actual = centro.menos(mitadPantalla);
        double actualX = actual.real;
        
        float completado = 0;
        float dCompletado = 1f / (w * h);
        for(int y = 0; y < h; y++) {
            for(int x = 0; x < w; x++, completado += dCompletado) {
                imagen.setRGB(x, y, coloreado.apply(actual).getRGB());
                if(Thread.currentThread().isInterrupted())
                    return null;
                actual.real += upp;
                reportero.accept(completado);
            }
            actual.imag -= upp;
            actual.real = actualX;
        }
        
        return imagen;
    }
    
    
    public static Color coloreadoHSB(
        int iteraciones,
        int ciclo,
        float hueOffset,
        Color fondo
    ) {
        if(iteraciones <= 0)
            return fondo;
        return Color.getHSBColor(
            (float)iteraciones / ciclo + hueOffset,
            1f,
            1f
        );
    }
}

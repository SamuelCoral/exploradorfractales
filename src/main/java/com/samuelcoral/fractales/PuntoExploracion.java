/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samuelcoral.fractales;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;
import javax.imageio.ImageIO;

/**
 *
 * @author samuel
 */
public class PuntoExploracion {
    
    public Complejo punto;
    public int iteraciones;
    public double upp;
    public float hueOffset;
    public int tonalidades;
    public Color fondo;

    public PuntoExploracion(
        Complejo punto,
        int iteraciones,
        double upp,
        float hueOffset,
        int tonalidades,
        Color fondo
    ) {
        this.punto = punto;
        this.iteraciones = iteraciones;
        this.upp = upp;
        this.hueOffset = hueOffset;
        this.tonalidades = tonalidades;
        this.fondo = fondo;
    }
    
    public PuntoExploracion() {
        this.punto = new Complejo();
        this.iteraciones = 250;
        this.upp = 0.0033;
        this.hueOffset = 0;
        this.tonalidades = 60;
        this.fondo = Color.BLACK;
    }
    
    
    public static void guardar(
        List<PuntoExploracion> exploracion,
        File rutaArchivo
    ) throws FileNotFoundException, IOException {
        try(
            FileOutputStream archivo = new FileOutputStream(rutaArchivo);
            ObjectOutputStream serializador = new ObjectOutputStream(archivo);
        ) {
            for(PuntoExploracion punto : exploracion) {
                serializador.writeDouble(punto.punto.real);
                serializador.writeDouble(punto.punto.imag);
                serializador.writeInt(punto.iteraciones);
                serializador.writeDouble(punto.upp);
                serializador.writeFloat(punto.hueOffset);
                serializador.writeInt(punto.tonalidades);
                serializador.writeInt(punto.fondo.getRGB());
            }
        }
    }
    
    public static LinkedList<PuntoExploracion> leer(File rutaArchivo)
        throws FileNotFoundException, IOException, ClassNotFoundException {
        try(
            FileInputStream archivo = new FileInputStream(rutaArchivo);
            ObjectInputStream serializador = new ObjectInputStream(archivo);
        ) {
            LinkedList<PuntoExploracion> puntos = new LinkedList<>();
            try {
                while(true) {
                    PuntoExploracion punto = new PuntoExploracion();
                    punto.punto.real = serializador.readDouble();
                    punto.punto.imag = serializador.readDouble();
                    punto.iteraciones = serializador.readInt();
                    punto.upp = serializador.readDouble();
                    punto.hueOffset = serializador.readFloat();
                    punto.tonalidades = serializador.readInt();
                    punto.fondo = new Color(serializador.readInt());
                    puntos.addLast(punto);
                }
            } catch(EOFException ex) {}
            return puntos;
        }
    }
    
    public static void exportar(
        LinkedList<PuntoExploracion> exploracion,
        File directorio,
        Dimension tamVideo,
        int framesPorPunto,
        Consumer<Float> reportarCompletadoTotal
    ) {
        // Interpolar la lista de puntos
        HashMap<Integer, PuntoExploracion> totales = new HashMap<>();
        PuntoExploracion anterior = exploracion.getFirst();
        int frameNum = 0;
        for(PuntoExploracion punto : exploracion) {
            if(punto == anterior) continue;
            
            float velInterp = 1f / framesPorPunto;
            float interpolado = 0;
            for(int i = 0; i < framesPorPunto; i++, interpolado += velInterp) {
                totales.put(frameNum++, new PuntoExploracion(
                    anterior.punto.mas(
                        punto.punto.menos(anterior.punto).por(interpolado)
                    ),
                    (int)(anterior.iteraciones +
                        interpolado * (punto.iteraciones - anterior.iteraciones)
                    ),
                    anterior.upp + interpolado * (punto.upp - anterior.upp),
                    anterior.hueOffset +
                        interpolado * (punto.hueOffset - anterior.hueOffset),
                    (int)(anterior.tonalidades +
                        interpolado * (punto.tonalidades - anterior.tonalidades)
                    ),
                    new Color(
                        (int)(anterior.fondo.getRed() + interpolado * (
                            punto.fondo.getRed() - anterior.fondo.getRed()
                        )),
                        (int)(anterior.fondo.getGreen() + interpolado * (
                            punto.fondo.getGreen() - anterior.fondo.getGreen()
                        )),
                            (int)(anterior.fondo.getBlue()+ interpolado * (
                            punto.fondo.getBlue()- anterior.fondo.getBlue()
                        ))
                    )
                ));
            }
            
            anterior = punto;
        }
        
        // Guardar todos los frames
        directorio.mkdir();
        String fmtArchivo = "%0" +
            String.valueOf((int)Math.log10(totales.size()) + 1) + "d.png";

        class Completado {
            
            public float acumulador;
            public float delta;
            
            public Completado(float acumulador, float delta) {
                this.acumulador = acumulador;
                this.delta = delta;
            }
            
            public synchronized float sumarDelta() {
                acumulador += delta;
                return acumulador;
            }
        }
        Completado completado = new Completado(0, 1f / totales.size());
        totales.entrySet().parallelStream().forEach(frame -> {
            int n = frame.getKey();
            PuntoExploracion punto = frame.getValue();
            BufferedImage imagen = Fractal.dibujar(
                actual -> Fractal.mandelbrot(
                    actual,
                    punto.iteraciones,
                    punto.tonalidades,
                    punto.hueOffset,
                    punto.fondo
                ),
                punto.punto,
                tamVideo,
                punto.upp,
                x -> {}
            );
            
            try {
                ImageIO.write(imagen, "png",
                    new File(directorio, String.format(fmtArchivo, n))
                );
            } catch(IOException ex) {}
            
            reportarCompletadoTotal.accept(completado.sumarDelta());
        });
    }
}
